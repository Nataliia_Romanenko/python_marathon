from src.practice1 import find
import collections


def test_without_pass():
	actual = find("without_pass.json", 'password')
	expected = []
	assert actual == expected


def test_one_user_array_pass():
	actual = find("one_user_array_pass.json", "password")
	expected = ['try', '_00_']
	assert collections.Counter(actual) == collections.Counter(expected)


def test_one_user_one_pass():
	actual = find("one_user_one_pass.json", "password")
	expected = ['_00_']
	assert collections.Counter(actual) == collections.Counter(expected)


def test_array_pass():
	actual = find("array_pass.json", "password")
	expected = ['56', '_00_']
	assert collections.Counter(actual) == collections.Counter(expected)
