from io import StringIO
from src.practice2 import parse_user
import os
import logging

#logging.basicConfig(filename='app.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
#logger = logging.getLogger(__name__)

# import pytest
#
# @pytest.fixture(name="log_output")
# def fixture_log_output():
# 	return LogCapture()
#
# @pytest.fixture(autouse=True)
# def capture():
# 	with LogCapture() as capture:
# 		yield capture

def print_file(file_name):
  with open(file_name, 'r') as f:
    for line in f.readlines():
      print(line, end = "")


def print_output(object):
	import sys
	old_stdout = sys.stdout
	sys.stdout = result = StringIO()
	print(object)
	sys.stdout = old_stdout
	return result.getvalue()

def read_file(file_name):
	with open(file_name, 'r') as f:
		return f.read()

def delete_file(file_name):
	if os.path.exists(file_name):
		os.remove(file_name)
		return f"file {file_name} removed"
	else:
		return f"file {file_name} doesn't exist"

def test_parse_user1_user2():
	parse_user("user4.json", "user1.json", "user2.json")
	actual = read_file("user4.json")
	expected = read_file("user1_user2.json")
	assert delete_file("user4.json") == "file user4.json removed" and actual == expected

def test_parse_user_without_name():
	parse_user("user4.json", "user_without_name.json")
	actual = read_file("user4.json")
	expected = read_file("expected_user_without_name.json")
	assert delete_file("user4.json") == "file user4.json removed" and actual == expected

def test_parse_user_with_duplicated_name():
	parse_user("user4.json", "user_with_duplicated_name.json")
	actual = read_file("user4.json")
	expected = read_file("expected_user_with_duplicated_name.json")
	assert delete_file("user4.json") == "file user4.json removed" and actual == expected

def test_parse_user2_user1():
	parse_user("user4.json", "user2.json", "user1.json")
	actual = read_file("user4.json")
	expected = read_file("user2_user1.json")
	assert delete_file("user4.json") == "file user4.json removed" and actual == expected

def test_parse_files_is_absent():
	parse_user("user4.json", "user_.json", "user_n.json")
	actual = read_file("user4.json")
	expected = read_file("expected_user_without_name.json")
	assert delete_file("user4.json") == "file user4.json removed" and actual == expected
